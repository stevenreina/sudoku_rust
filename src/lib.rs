pub mod board;
pub mod chunk;
pub mod value;

use std::fmt::Display;

use serde::ser::SerializeMap;

use crate::{board::Board, value::ValueOption};

pub trait Generator {
    fn generate() -> Self;
}

#[derive(Debug)]
pub enum InputError {
    OutOfBounds,
}
impl Display for InputError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let explanation = match self {
            InputError::OutOfBounds => "Index out of board bounds.",
        };
        f.write_str(explanation)
    }
}

pub struct SudokuGame {
    board: Board,
    modifyable: [bool; 81],
    moves: Vec<(ValueOption, usize)>,
}
impl SudokuGame {
    pub fn new(board: Board) -> Self {
        let mut modifyable = [true; 81];
        board
            .get_values()
            .iter()
            .enumerate()
            .for_each(|(idx, item)| {
                if *item != ValueOption::None {
                    modifyable[idx] = false;
                }
            });
        Self {
            board,
            modifyable,
            moves: vec![],
        }
    }
    pub fn get_board(&self) -> &Board {
        &self.board
    }
    pub fn get_mut_board(&mut self) -> &mut Board {
        &mut self.board
    }
    pub fn set_board(&mut self, board: Board) {
        self.modifyable.fill_with(|| true);
        board
            .get_values()
            .iter()
            .enumerate()
            .for_each(|(idx, value)| {
                if *value != ValueOption::None {
                    self.modifyable[idx] = false
                }
            });
        self.board = board;
    }
    pub fn get_modifyable(&self) -> &[bool; 81] {
        &self.modifyable
    }
    pub fn get_mut_modifyable(&mut self) -> &mut [bool; 81] {
        &mut self.modifyable
    }
    pub fn reset_modifyable(&mut self) {
        self.modifyable = [true; 81];
    }
    pub fn set_modifyable(&mut self, new_modifyable: [bool; 81]) {
        self.modifyable = new_modifyable;
    }
    pub fn get_moves(&self) -> &Vec<(ValueOption, usize)> {
        &self.moves
    }
    pub fn get_mut_moves(&mut self) -> &mut Vec<(ValueOption, usize)> {
        &mut self.moves
    }
    pub fn reset_moves(&mut self) {
        self.moves = vec![];
    }
    pub fn set_moves(&mut self, new_moves: Vec<(ValueOption, usize)>) {
        self.moves = new_moves;
    }
    pub fn make_modifyable_by_current_board(&mut self) {
        self.board
            .get_values()
            .iter()
            .enumerate()
            .for_each(|(idx, value)| {
                if *value != ValueOption::None {
                    self.modifyable[idx] = false
                }
            });
    }
    pub fn play(&mut self, value: ValueOption, index: usize) -> Result<(), InputError> {
        if let Some(is_modifyable) = self.modifyable.get(index) {
            if *is_modifyable {
                self.moves.push((value, index));
                self.board.set_box(index, value)?;
                return Ok(());
            }
        };
        Err(InputError::OutOfBounds)
    }
}
impl serde::Serialize for SudokuGame {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(3))?;
        map.serialize_entry("board", self.get_board())?;
        map.serialize_entry("modifyable", &self.get_modifyable().to_vec())?;
        map.serialize_entry("moves", self.get_moves())?;
        map.end()
    }
}
impl Generator for SudokuGame {
    fn generate() -> Self {
        let board = Board::generate();
        let mut modifyable = [true; 81];
        board
            .get_values()
            .iter()
            .enumerate()
            .for_each(|(idx, r#box)| {
                if *r#box != ValueOption::None {
                    modifyable[idx] = false;
                }
            });
        Self {
            board,
            modifyable,
            moves: vec![],
        }
    }
}
