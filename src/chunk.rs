use std::collections::HashSet;

use crate::value::ValueOption;

pub type Chunk = [ValueOption; 9];

pub fn has_repetitions(chunk: Chunk) -> bool {
    let mut value_set = HashSet::with_capacity(9);
    for r#box in chunk {
        if r#box != ValueOption::None && !value_set.insert(r#box) {
            println!("{chunk:?}");
            return true;
        }
    }
    false
}

/* | _ _ _ | _ _ _ | _ _ _ | */
pub const EMPTY_CHUNK: Chunk = [
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
];

/* | 1 2 3 | 4 5 6 | 7 8 9 | */
pub const ONE_THROUGH_NINE: Chunk = [
    ValueOption::One,
    ValueOption::Two,
    ValueOption::Three,
    ValueOption::Four,
    ValueOption::Five,
    ValueOption::Six,
    ValueOption::Seven,
    ValueOption::Eight,
    ValueOption::Nine,
];

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn chunk_has_repetitions() {
        let mut chunk = ONE_THROUGH_NINE;
        chunk[1] = ValueOption::One;
        assert!(has_repetitions(chunk))
    }

    #[test]
    fn chunk_has_no_repetitions() {
        let chunk = ONE_THROUGH_NINE;
        assert!(!has_repetitions(chunk));
    }
}
