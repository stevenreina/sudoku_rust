use std::fmt::Display;

use serde::Serialize;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum ValueOption {
    None,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
}
impl Display for ValueOption {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let char = match self {
            ValueOption::None => '_',
            ValueOption::One => '1',
            ValueOption::Two => '2',
            ValueOption::Three => '3',
            ValueOption::Four => '4',
            ValueOption::Five => '5',
            ValueOption::Six => '6',
            ValueOption::Seven => '7',
            ValueOption::Eight => '8',
            ValueOption::Nine => '9',
        };
        f.write_str(&format!("{char}"))
    }
}
impl Serialize for ValueOption {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::ValueOption;
    use serde::Serialize;
    use serde_json::Serializer;

    #[test]
    fn value_option_format() {
        assert_eq!(ValueOption::None.to_string(), "_")
    }

    #[test]
    fn serialized_format() {
        let mut buf = Vec::with_capacity(1);
        let mut pretty_serializer = Serializer::pretty(&mut buf);
        ValueOption::None.serialize(&mut pretty_serializer).unwrap();
        let a = String::from_utf8(buf).unwrap();
        assert_eq!(&a, "\"_\"")
    }
}
