use std::fmt::Display;

use serde::{ser::SerializeMap, Serialize};

use crate::{chunk::*, value::ValueOption, Generator, InputError};

#[derive(Clone, Copy, Debug, PartialEq, Serialize)]
pub enum BoardStatus {
    Invalid,
    NotStarted,
    Unfinished,
    Complete,
}

pub type BoardValues = [ValueOption; 81];

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Board {
    values: BoardValues,
    status: BoardStatus,
}
impl Board {
    pub fn update_status(&mut self) {
        if !self.is_valid() {
            self.set_status(BoardStatus::Invalid);
        } else if self.is_complete() {
            self.set_status(BoardStatus::Complete);
        } else {
            self.set_status(BoardStatus::Unfinished);
        }
    }
    pub fn new(values: BoardValues) -> Self {
        let mut instance = Self {
            values,
            status: BoardStatus::NotStarted,
        };
        instance.update_status();
        instance
    }
    pub fn get_values(&self) -> BoardValues {
        self.values
    }
    pub fn get_mut_values(&mut self) -> &mut BoardValues {
        &mut self.values
    }
    pub fn set_values(&mut self, values: BoardValues) {
        self.values = values;
        self.update_status();
    }
    pub fn get_status(&self) -> BoardStatus {
        self.status
    }
    pub fn get_mut_status(&mut self) -> &mut BoardStatus {
        &mut self.status
    }
    pub fn set_status(&mut self, status: BoardStatus) {
        self.status = status;
    }
    pub fn is_valid(&self) -> bool {
        for chunk_position in 0..9 {
            if has_repetitions(self.get_vertical_chunk(chunk_position)) {
                // eprintln!("Repetitions in vertical chunk, X Position = {chunk_position}");
                return false;
            } else if has_repetitions(self.get_horizontal_chunk(chunk_position)) {
                // eprintln!("Repetitions in horizontal chunk, Y Position = {chunk_position}");
                return false;
            } else if has_repetitions(self.get_block_chunk(chunk_position)) {
                // eprintln!("Repetitions in block chunk, Block Position = {chunk_position}");
                return false;
            }
        }
        true
    }
    pub fn is_complete(&self) -> bool {
        self.is_valid() && self.values.iter().all(|item| *item != ValueOption::None)
    }
    // TODO: fn is_solvable(&self) -> bool { todo!() }
    pub fn get_horizontal_chunk(&self, index_y: usize) -> Chunk {
        let offset = 9 * index_y;
        let mut chunk: Chunk = [ValueOption::None; 9];
        chunk.iter_mut().enumerate().for_each(|(idx, r#box)| {
            *r#box = self.values[offset + idx];
        });
        chunk
    }
    pub fn set_horizontal_chunk(&mut self, index_y: usize, chunk: Chunk) {
        let offset = 9 * index_y;
        self.values[offset..offset + 9]
            .iter_mut()
            .enumerate()
            .for_each(|(idx, r#box)| {
                *r#box = chunk[idx];
            });
    }
    pub fn get_vertical_chunk(&self, index_x: usize) -> Chunk {
        let mut chunk: Chunk = [ValueOption::None; 9];
        let reference: Vec<ValueOption> = self
            .values
            .iter()
            .enumerate()
            .filter_map(|(idx, item)| {
                if idx <= index_x {
                    if idx == index_x {
                        return Some(*item);
                    }
                } else {
                    if (idx - index_x) % 9 == 0 {
                        return Some(*item);
                    }
                }
                None
            })
            .collect();
        chunk.iter_mut().enumerate().for_each(|(idx, r#box)| {
            *r#box = reference[idx];
        });
        chunk
    }
    pub fn set_vertical_chunk(&mut self, index_x: usize, chunk: Chunk) {
        self.values
            .iter_mut()
            .enumerate()
            .filter_map(|(idx, item)| {
                if idx <= index_x {
                    if idx == index_x {
                        return Some(item);
                    }
                } else {
                    if (idx - index_x) % 9 == 0 {
                        return Some(item);
                    }
                }
                None
            })
            .enumerate()
            .for_each(|(idx, r#box)| *r#box = chunk[idx]);
    }

    fn get_block_chunk_position(block_position: usize) -> Result<usize, InputError> {
        match block_position {
            0..=2 => Ok(3 * block_position),
            3..=5 => Ok(27 + 3 * (block_position - 3)),
            6..=8 => Ok(54 + 3 * (block_position - 6)),
            _ => Err(InputError::OutOfBounds),
        }
    }

    pub fn get_block_chunk(&self, chunk_position: usize) -> Chunk {
        let mut chunk: Chunk = [ValueOption::None; 9];
        let block_init_position =
            Self::get_block_chunk_position(chunk_position).expect("Bad block chunk position");
        for count in 0..3 {
            chunk[3 * count..3 * count + 3]
                .iter_mut()
                .enumerate()
                .for_each(|(idx, item)| {
                    *item = self.values[block_init_position + idx + 9 * count];
                });
        }
        chunk
    }

    pub fn set_block_chunk(
        &mut self,
        block_position: usize,
        chunk: Chunk,
    ) -> Result<(), InputError> {
        let block_init_position = Self::get_block_chunk_position(block_position)?;
        self.get_mut_values()
            .iter_mut()
            .enumerate()
            .filter_map(|(idx, value)| {
                if (block_init_position..block_init_position + 3).contains(&idx)
                    || ((block_init_position + 9)..(block_init_position + 9) + 3).contains(&idx)
                    || ((block_init_position + 18)..(block_init_position + 18) + 3).contains(&idx)
                {
                    Some(value)
                } else {
                    None
                }
            })
            .enumerate()
            .for_each(|(idx, value)| *value = chunk[idx]);
        Ok(())
    }
    pub fn set_box(&mut self, index: usize, value: ValueOption) -> Result<(), InputError> {
        if index > 80 {
            return Err(InputError::OutOfBounds);
        }
        self.get_mut_values()[index] = value;
        Ok(())
    }
}
impl Default for Board {
    fn default() -> Self {
        Self {
            values: [ValueOption::None; 81],
            status: BoardStatus::Invalid,
        }
    }
}
impl Generator for Board {
    fn generate() -> Self {
        Self::default()
    }
}
impl Serialize for Board {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(2))?;
        map.serialize_entry("status", &self.status)?;
        map.serialize_entry("board", &self.values.to_vec())?;
        map.end()
    }
}
impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut format = String::new();
        self.values.iter().enumerate().for_each(|(idx, &value)| {
            format.push_str(&format!(" {value}"));
            if (idx + 1) % 9 == 0 {
                format.push('\n');
                if (idx + 1) % 27 == 0 && (idx + 1) != 81 {
                    format.push_str(&format!(" {}\n", format!("–").repeat(21)));
                }
                return;
            }
            if (idx + 1) % 3 == 0 {
                format.push_str(" |");
            }
        });
        f.write_str(&format)
    }
}

/*
 *  _ 7 _ | _ _ 2 | _ 4 _
 *  _ _ 3 | _ _ _ | 6 _ 7
 *  _ _ 4 | 3 _ _ | _ _ _
 *  ––––––––––––––––––––––
 *  7 6 _ | 8 _ _ | _ _ 9
 *  _ _ _ | _ _ 4 | 7 _ 1
 *  _ 1 _ | _ 6 _ | _ _ _
 *  ––––––––––––––––––––––
 *  _ 4 6 | _ _ _ | _ _ _
 *  _ 5 _ | _ 8 3 | _ _ _
 *  3 _ _ | _ 4 _ | 8 1 _
 *
 */
pub const SUDOKU_BOARD_1: BoardValues = [
    ValueOption::None,
    ValueOption::Seven,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Two,
    ValueOption::None,
    ValueOption::Four,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Three,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Six,
    ValueOption::None,
    ValueOption::Seven,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Four,
    ValueOption::Three,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Seven,
    ValueOption::Six,
    ValueOption::None,
    ValueOption::Eight,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Nine,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Four,
    ValueOption::Seven,
    ValueOption::None,
    ValueOption::One,
    ValueOption::None,
    ValueOption::One,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Six,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Four,
    ValueOption::Six,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Five,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Eight,
    ValueOption::Three,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Three,
    ValueOption::None,
    ValueOption::None,
    ValueOption::None,
    ValueOption::Four,
    ValueOption::None,
    ValueOption::Eight,
    ValueOption::One,
    ValueOption::None,
];

#[cfg(test)]
mod tests {
    use crate::board::*;

    /* ACQUISITION */

    #[test]
    fn get_row() {
        let board = Board::new(SUDOKU_BOARD_1);
        /* | 7 6 _ | 8 _ _ | _ _ 9 | */
        let mut reference = EMPTY_CHUNK;
        reference[0] = ValueOption::Seven;
        reference[1] = ValueOption::Six;
        reference[3] = ValueOption::Eight;
        reference[8] = ValueOption::Nine;

        assert_eq!(board.get_horizontal_chunk(3), reference)
    }
    #[test]
    fn get_column() {
        let board = Board::new(SUDOKU_BOARD_1);
        /*
         *  7
         *  _
         *  _
         *
         *  6
         *  _
         *  1
         *
         *  4
         *  5
         *  _
         */
        let mut reference = EMPTY_CHUNK;
        reference[0] = ValueOption::Seven;
        reference[3] = ValueOption::Six;
        reference[5] = ValueOption::One;
        reference[6] = ValueOption::Four;
        reference[7] = ValueOption::Five;

        assert_eq!(board.get_vertical_chunk(1), reference)
    }
    #[test]
    fn get_block() {
        let board = Board::new(SUDOKU_BOARD_1);
        /*
         * | _ _ _ |
         * | _ 8 3 |
         * | _ 4 _ |
         *
         */
        let mut reference = EMPTY_CHUNK;
        reference[4] = ValueOption::Eight;
        reference[5] = ValueOption::Three;
        reference[7] = ValueOption::Four;

        assert_eq!(board.get_block_chunk(7), reference)
    }

    /* UPDATING */
    #[test]
    fn set_row() {
        let mut board = Board::new(SUDOKU_BOARD_1);
        let new_row = ONE_THROUGH_NINE;
        let chunk_position = 7;
        board.set_horizontal_chunk(chunk_position, new_row);
        assert_eq!(
            &new_row,
            &board.get_values()[9 * chunk_position..9 * (chunk_position + 1)]
        )
    }
    #[test]
    fn set_column() {
        let mut board = Board::new(SUDOKU_BOARD_1);
        let new_column = ONE_THROUGH_NINE;
        let chunk_position = 7;
        board.set_vertical_chunk(chunk_position, new_column);
        let updated_column = board
            .get_values()
            .iter()
            .enumerate()
            .filter_map(|(idx, value)| {
                if idx <= chunk_position {
                    if idx == chunk_position {
                        return Some(*value);
                    }
                } else {
                    if (idx - chunk_position) % 9 == 0 {
                        return Some(*value);
                    }
                }
                None
            })
            .collect::<Vec<ValueOption>>();
        assert_eq!(&new_column, updated_column.as_slice())
    }
    #[test]
    fn set_block() {
        let mut board = Board::new(SUDOKU_BOARD_1);
        let new_block = ONE_THROUGH_NINE;
        let chunk_position = 7;
        board.set_block_chunk(chunk_position, new_block).unwrap();
        assert_eq!(board.get_block_chunk(7), ONE_THROUGH_NINE);
    }

    /* VALIDATION */

    #[test]
    fn is_valid() {
        let board = Board::new(SUDOKU_BOARD_1);
        assert!(board.is_valid())
    }
    #[test]
    fn is_not_valid() {
        let mut board = Board::new(SUDOKU_BOARD_1);
        board.get_mut_values()[0..9]
            .iter_mut()
            .for_each(|r#box| *r#box = ValueOption::One);
        assert!(!board.is_valid())
    }

    /* FORMAT */

    #[test]
    fn has_correct_display_format() {
        let reference = format!(
            "{}{}{}{}{}{}{}{}{}{}{}",
            " _ 7 _ | _ _ 2 | _ 4 _\n",
            " _ _ 3 | _ _ _ | 6 _ 7\n",
            " _ _ 4 | 3 _ _ | _ _ _\n",
            " –––––––––––––––––––––\n",
            " 7 6 _ | 8 _ _ | _ _ 9\n",
            " _ _ _ | _ _ 4 | 7 _ 1\n",
            " _ 1 _ | _ 6 _ | _ _ _\n",
            " –––––––––––––––––––––\n",
            " _ 4 6 | _ _ _ | _ _ _\n",
            " _ 5 _ | _ 8 3 | _ _ _\n",
            " 3 _ _ | _ 4 _ | 8 1 _\n",
        );
        assert_eq!(Board::new(SUDOKU_BOARD_1).to_string(), reference);
    }
}
